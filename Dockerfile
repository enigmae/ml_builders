# Note - Multi Stage Builds Require Docker 17.05
# Befor Multi-Stage builds only one FROM was alloewed in a Dockerfile

# Stage One: create the python image
FROM python:3.7.7-buster
RUN mkdir -p app
WORKDIR app

COPY autotrainz  .
COPY requirements.txt .

RUN pip install -r requirements.txt

# CMD python {{executable_app}}

# Stage two: Merge from DinD
FROM docker:stable-dind

# Stage Three: gcloud for kubectl
FROM google/cloud-sdk:latest
