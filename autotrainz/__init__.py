# Copyright © 2020 Hashmap, Inc
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import platform
import os

if not os.getenv('AUTOTRAINZ_HOME'):
    if platform.system().lower() != 'windows':
        os.environ['AUTOTRAINZ_HOME'] = os.getenv('HOME')
    else:
        os.environ['AUTOTRAINZ_HOME'] = os.getenv('USERPROFILE')

import providah.factories.package_factory as pf


pf.PackageFactory.fill_registry()
